﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	public float m_LifeTime = 0.1f;

	// Use this for initialization
	private void Start () {
		
	}
	
	// Update is called once per frame
	private void Update () {
		
	}

	private void Awake() {
		Destroy (gameObject, m_LifeTime);
	}
}
