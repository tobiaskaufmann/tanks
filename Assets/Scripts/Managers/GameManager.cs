﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	public int m_NumRoundsToWin = 5;
	public float m_StartDelay = 3f;
	public float m_EndDelay = 3f;
	public CameraControl m_CameraControl;
	public Text m_MessageText;
	public GameObject m_TankPrefab;
	public TankManager[] m_Tanks;
	public Button m_ButtonStart;
	public Button m_ButtonExit;
	public AudioSource m_SelectionAudio;
	public AudioClip m_SelectionAudioClip;
	public AudioSource m_MenuAudio;
	public AudioClip m_MenuAudioClip; 

	[HideInInspector] public bool m_GameStarted;

	private int m_RoundNumber;
	private WaitForSeconds m_StartWait;
	private WaitForSeconds m_EndWait;
	private TankManager m_RoundWinner;
	private TankManager m_GameWinner;
	private IEnumerator m_GameLoopCoroutineObject;
	private bool menuActivated = false;
	private bool roundPlaying = false;

	private string pauseButton = "Pause";

	private void Start ()
	{
		m_StartWait = new WaitForSeconds (m_StartDelay);
		m_EndWait = new WaitForSeconds (m_EndDelay);

	}

	private void Update ()
	{
		if (Input.GetButtonDown (pauseButton) && m_GameStarted && !menuActivated && roundPlaying) {
			menuActivated = true;
			m_MenuAudio.clip = m_MenuAudioClip;
			m_MenuAudio.Play ();	
			m_MessageText.text = "PAUSE";
			m_ButtonExit.gameObject.SetActive (true);
			m_ButtonStart.gameObject.SetActive (true);
			DisableTankControl ();

		} else if (Input.GetButtonDown (pauseButton) && m_GameStarted && menuActivated && roundPlaying) {
			menuActivated = false;
			m_MenuAudio.clip = m_MenuAudioClip;
			m_MenuAudio.Play ();	
			m_MessageText.text = "";
			m_ButtonExit.gameObject.SetActive (false);
			m_ButtonStart.gameObject.SetActive (false);

			EnableTankControl ();
		}
	}


	private void SpawnAllTanks ()
	{
		for (int i = 0; i < m_Tanks.Length; i++) {
			m_Tanks [i].m_Instance =
                Instantiate (m_TankPrefab, m_Tanks [i].m_SpawnPoint.position, m_Tanks [i].m_SpawnPoint.rotation) as GameObject;
			m_Tanks [i].m_PlayerNumber = i + 1;
			m_Tanks [i].Setup ();
		}
	}


	private void SetCameraTargets ()
	{
		Transform[] targets = new Transform[m_Tanks.Length];

		for (int i = 0; i < targets.Length; i++) {
			targets [i] = m_Tanks [i].m_Instance.transform;
		}

		m_CameraControl.m_Targets = targets;
	}


	private IEnumerator GameLoop ()
	{
		yield return StartCoroutine (RoundStarting ());
		yield return StartCoroutine (RoundPlaying ());
		yield return StartCoroutine (RoundEnding ());

		if (m_GameWinner != null) {
			Application.LoadLevel (Application.loadedLevel);
		} else {
			m_GameLoopCoroutineObject = GameLoop ();
			StartCoroutine (m_GameLoopCoroutineObject);
		}
	}


	private IEnumerator RoundStarting ()
	{
		roundPlaying = false;
		ResetAllTanks ();
		DisableTankControl ();

		m_CameraControl.SetStartPositionAndSize ();

		m_RoundNumber++;
		m_MessageText.text = "ROUND " + m_RoundNumber;

		yield return m_StartWait;
	}


	private IEnumerator RoundPlaying ()
	{
		roundPlaying = true; 
		EnableTankControl ();
		m_MessageText.text = string.Empty;

		while (!OneTankLeft ()) {

			yield return null;
		}

	}


	private IEnumerator RoundEnding ()
	{
		roundPlaying = false;
		DisableTankControl ();
		m_RoundWinner = null;
		m_RoundWinner = GetRoundWinner ();

		if (m_RoundWinner != null)
			m_RoundWinner.m_Wins++;

		m_GameWinner = GetGameWinner ();

		string message = EndMessage ();
		m_MessageText.text = message;	
		
		yield return m_EndWait;
	}


	private bool OneTankLeft ()
	{
		int numTanksLeft = 0;

		for (int i = 0; i < m_Tanks.Length; i++) {
			if (m_Tanks [i].m_Instance.activeSelf)
				numTanksLeft++;
		}

		return numTanksLeft <= 1;
	}


	private TankManager GetRoundWinner ()
	{
		for (int i = 0; i < m_Tanks.Length; i++) {
			if (m_Tanks [i].m_Instance.activeSelf)
				return m_Tanks [i];
		}

		return null;
	}


	private TankManager GetGameWinner ()
	{
		for (int i = 0; i < m_Tanks.Length; i++) {
			if (m_Tanks [i].m_Wins == m_NumRoundsToWin)
				return m_Tanks [i];
		}

		return null;
	}

	private void ResetCounters ()
	{
		m_RoundNumber = 0;
		for (int i = 0; i < m_Tanks.Length; i++) {
			m_Tanks [i].m_Wins = 0;
		}
	}


	private string EndMessage ()
	{
		string message = "DRAW!";

		if (m_RoundWinner != null)
			message = m_RoundWinner.m_ColoredPlayerText + " WINS THE ROUND!";

		message += "\n\n\n\n";

		for (int i = 0; i < m_Tanks.Length; i++) {
			message += m_Tanks [i].m_ColoredPlayerText + ": " + m_Tanks [i].m_Wins + " WINS\n";
		}

		if (m_GameWinner != null)
			message = m_GameWinner.m_ColoredPlayerText + " WINS THE GAME!";

		return message;
	}


	private void ResetAllTanks ()
	{
		for (int i = 0; i < m_Tanks.Length; i++) {
			m_Tanks [i].Reset ();
		}
	}


	private void EnableTankControl ()
	{
		for (int i = 0; i < m_Tanks.Length; i++) {
			m_Tanks [i].EnableControl ();
		}
	}


	private void DisableTankControl ()
	{
		for (int i = 0; i < m_Tanks.Length; i++) {
			m_Tanks [i].DisableControl ();
		}
	}

	public void OnClickNewGameBtn ()
	{

		m_SelectionAudio.clip = m_SelectionAudioClip;
		m_SelectionAudio.Play ();	

		if (!m_GameStarted) {
			SpawnAllTanks ();
			SetCameraTargets ();
		} else {
			StopCoroutine (m_GameLoopCoroutineObject);
			ResetCounters ();
		}
		m_GameLoopCoroutineObject = GameLoop ();
		StartCoroutine (m_GameLoopCoroutineObject);

		m_ButtonExit.gameObject.SetActive (false);
		m_ButtonStart.gameObject.SetActive (false);
		m_GameStarted = true;
	}

	public void OnClickExitGameBtn() {
		m_SelectionAudio.clip = m_SelectionAudioClip;
		m_SelectionAudio.Play ();	

		Application.Quit ();
	}
}