﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TankShooting : MonoBehaviour
{
	public int m_PlayerNumber = 1;
	public Rigidbody m_Shell;
	public Rigidbody m_Bullet;
	public Transform m_FireTransform;
	public Slider m_AimSlider;
	public AudioSource m_ShootingAudio;
	public AudioSource m_ShellAudio;
	public AudioClip m_ChargingClip;
	public AudioClip m_FireClip;
	public AudioClip m_FireSecondaryClip;
	public AudioClip m_ShellSecondaryClip;
	public float m_MinLaunchForce = 30f;
	public float m_MaxLaunchForce = 100f;
	public float m_MaxChargeTime = 1f;
	public float m_RoundsPerMinute = 200f;
	public float m_SecondaryLaunchForceFactor = 2f;
    
	private string m_FireButton;
	private string m_FireButtonSecondary;
	private float m_CurrentLaunchForce;
	private float m_ChargeSpeed;
	private bool m_Fired;

	private float m_CoolDownTime = 2f;
	private bool m_FiredSecondary = false;
	private bool m_FiringAutoStarted;
	private bool m_ReadyToFireSecondary = true;


	private void OnEnable ()
	{
		m_CurrentLaunchForce = m_MinLaunchForce;
		m_AimSlider.value = m_MinLaunchForce;
	}


	private void Start ()
	{
		m_FireButton = "Fire" + m_PlayerNumber;
		m_FireButtonSecondary = "Fire" + m_PlayerNumber + m_PlayerNumber;

		m_ChargeSpeed = (m_MaxLaunchForce - m_MinLaunchForce) / m_MaxChargeTime;

		StartCoroutine (CoolDownSecondary (true, 0f));
	}

	private void Update ()
	{
		// Track the current state of the fire button and make decisions based on the current launch force.
		m_AimSlider.value = m_MinLaunchForce;

		if (m_CurrentLaunchForce >= m_MaxLaunchForce && !m_Fired) {
			//at max charge, not yet fired
			m_CurrentLaunchForce = m_MaxLaunchForce;
			m_AimSlider.value = m_MaxLaunchForce;
			if (Input.GetButtonUp (m_FireButton) && !m_Fired)
				Fire ();

		} else if (Input.GetButtonDown (m_FireButton)) {
			//have we pressed fire for the first time?
			m_Fired = false;
			m_CurrentLaunchForce = m_MinLaunchForce;

			//m_ShootingAudio.clip = m_ChargingClip;
			//m_ShootingAudio.Play ();	

		} else if (Input.GetButton (m_FireButton) && !m_Fired) {
			//holding the fire button, not yet fired
			m_CurrentLaunchForce += m_ChargeSpeed * Time.deltaTime; 
			m_AimSlider.value = m_CurrentLaunchForce; 

		} else if (Input.GetButtonUp (m_FireButton) && !m_Fired) {
			//released the button, not yet fired 
			if (m_CurrentLaunchForce >= m_MaxLaunchForce / 2)
				Fire ();
		}


		if (Input.GetButton (m_FireButtonSecondary) && !m_FiredSecondary && m_ReadyToFireSecondary) {
			m_FiredSecondary = true;
			if (m_FiringAutoStarted) {
				Invoke ("FireSecondary", 1 / (m_RoundsPerMinute / 60));
			} else {
				m_FiringAutoStarted = true;
				FireSecondary ();	
			}
		} else if (Input.GetButtonUp (m_FireButtonSecondary)) {

			m_ShellAudio.clip = m_ShellSecondaryClip;
			m_ShellAudio.Play ();

			CoolDownSecondary (false, m_CoolDownTime);
		}
	}

	private IEnumerator CoolDownSecondary (bool init, float seconds)
	{
		if (!init) {
			m_FiringAutoStarted = false;
			m_ReadyToFireSecondary = false;

			yield return new WaitForSeconds (seconds);
			m_ReadyToFireSecondary = true;
		}
	}


	private void Fire ()
	{

		// Instantiate and launch the shell.
		m_Fired = true;

		Rigidbody shellInstance = Instantiate (m_Shell, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;

		shellInstance.velocity = m_CurrentLaunchForce * m_FireTransform.forward;

		m_ShootingAudio.clip = m_FireClip;
		m_ShootingAudio.Play ();


		m_CurrentLaunchForce = m_MinLaunchForce;
	}

	private void FireSecondary ()
	{
		// Instantiate and launch the shell.
		m_FiredSecondary = false;
		float factor = m_SecondaryLaunchForceFactor - (Random.value / 3);

		Rigidbody bulletInstance = Instantiate (m_Bullet, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;

		bulletInstance.velocity = m_CurrentLaunchForce * m_FireTransform.forward * factor;

		m_ShootingAudio.clip = m_FireSecondaryClip;
		m_ShootingAudio.Play ();

		m_CurrentLaunchForce = m_MinLaunchForce;
	}
}